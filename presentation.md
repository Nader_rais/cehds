# CEH Security Report 
#### written by Nader_Rais aka [Nerosiar](https://github.com/nerosiar)
#### Framed by [Moez Bouzayani](https://www.linkedin.com/in/moez-bouzayani/)
---

<!-- .slide: data-background-video="https://cdn.flixel.com/flixel/a3y113qg5bay3j3lp3t7.hd.mp4" data-background-video-loop="loop" data-background-video-muted -->

# WELCOME!

---
<!-- .slide: data-background-video="https://cdn.flixel.com/flixel/iy6gmpppiksf3idk2nnn.hd.mp4" data-background-video-loop="loop" data-background-video-muted -->
## Pentesting == twilling the pen 
> To perform the penetration testing procedure , we need to write a security report 
---
# Overview
---
* [x] Introduction
  * [x] Cyber-security Active Parts
  * [x] Security incidents e-commerce  
* [x] Foot-Printing
  * [x] Metadata 
  * [x] Recon
  * [x] GPS 
  * [x] Screenshot of the same location!
* [x] Reconnaissance
* [x] Sequel Injection
* [x] Social Engineering
* [x] Analysing Network Traffic Using Wireshark
 [x] Open space, comments

---


## Introduction :
---
### Cybersec Active Parts 

* [CISA: Cybersecurity and Infrastructure Security Agency](https://https://www.cisa.gov/) : 
<img src="https://www.cisa.gov/sites/default/files/cisa/logo_verbose.png" alt="CISA">

----

This Agency was established on Nov 16, 2018 . it's a new standalone of the federal American agencies , Trump made from this agency an operational component under Department of Homeland Security , their main activities is the control of the cyber world ! In 2020 CISA created a website to rebut desinformation associated with the US presidential election . Trump released this Agency in NOv 16 2018 - and On Nov 12 ,2020 , 2 years exactly , CISA issued a press saying that :
> "There is no evidence that any voting system deleted or lost votes, changed votes, or was in any way compromised." 

Trump posted a twitt last 2 weeks very angry of Mr Bryan Ware the assistant director at CISA ,and this agency i think blocked the Trump twitter account ! hhhhhhh 😂
----
* [ANSI: Agence Nationale de la Sécurité Informatique ](https://www.ansi.tn/)
<img src="https://www.ansi.tn/sites/all/themes/adaptivetheme/ansi_theme/logo.png" alt="ANSI">
----
The National Agency of Computer Security of Tunisia , is a devision of the Tunisian governement of Technologies , The ANSI mission is to collaborate with the private sector , governement , military and intelligence stakeholders to conduct risk assessments and mitigate vulnerabilities and threats to information technology assets and activities . 
(i don't know if that true just i think that they should be like what i say )
Anyway they are actiive and productive , in the corona virus they send messages to tunisian people inform them about the cyberSecurity incidents in the COVID period and they should take care about the cybersafety.

----

<img src="img/Screenshot1.png" alt="ANSI">
<img src="https://scontent.ftun10-1.fna.fbcdn.net/v/t1.0-0/s350x350/125112709_1750485181800720_2392522508761010769_o.png?_nc_cat=109&ccb=2&_nc_sid=730e14&_nc_ohc=VGkK2bBB0ZcAX_Lsjf0&_nc_ht=scontent.ftun10-1.fna&oh=d2086f2b38dcc518e86d4652a8ff863a&oe=5FDEBCBC" alt="ANSI">

---

### E-commerce Security incidents 

----

  * [Marijuana Dispensaries January 23, 2020](https://www.vpnmentor.com/blog/report-thsuite-breach/)
  One of the famous e-commerce website in the world , this website is an open market for cannabis .
  This point-of-sale system of marijuana dispensaries across the U.S., disclosed personal information belonging to over 85,000 medical marijuana patients and recreational users after leaving their database unprotected. The data breach impacted names, date of births, phone numbers, emails, street addresses, patient names and medical ID numbers, cannabis variety and the quantity purchased, total transaction costs, date received, and photographs of scanned government and employee IDs.
  it's a dangerous cyberattack while the data stolen is priceless , this type of data can attend thousands of Bitcoins in the dark net ...
  
----


  for more information search about THSuite in google , this e-commerce cannabis website have their own ERP solution it's a wonderful system
  I know that what known about  THSuite that they are a management and system of patient management but they have their own e-commerce platform on the darknet , the e-commerce platform was hacked in 2015 and every year the blackhat hackers try to breach this marketplace and in 2020 the e-commerce platform was successfully hacked but the data breached till now unknown where !!

----

  * [Booking.com & Hotels.com, November 6,2020](https://www.websiteplanet.com/blog/prestige-soft-breach-report/)
  In the start of November An unsecured database belonging to the hotel reservation platform, Prestige Software, leaked sensitive data from over 10 million hotel guests worldwide, dating as far back as 2013. The third-party data leak affected guests that have booked reservations through travel companies such as Expedia, Hotels.com, Booking.com, Agoda, Amadeus, Hotelbeds, Omnibees, Sabre and more. The information exposed in the data leak includes names, email addresses, national ID numbers, phone numbers of hotel guests, and reservation details such as reservation number, dates of a stay, the price paid per night. The unsecured database also disclosed sensitive credit card details from over 100,000 guests, including card number, cardholder’s name, CVV, and expiration data, and total cost of hotel reservations.
  It was one of the powerful Breach leaking attacks , there is a lot of e-commerce websites of the Giant group of Prestige Software !

----

  * [JM Bullion November 3 , 2020](https://www.techradar.com/news/this-could-be-the-most-expensive-data-breach-ever)
  Malware embedded in the online shopping platform of precious metals dealer, JM Bullion, captured the personal and banking card information of customers who made purchases between February and July 2020. Using the malicious code, hackers we able to collect an undisclosed number of customer names, addresses, and payment card details including account numbers, card expiration dates, and the security codes. I have to mention here that this platform and e-commerce website is the most high price products , so it's too interesting that a company like this don't implement a best security system.
  here an example of the products : 

----
<img src=img/Screenshot3.png alt="barcha flouss">

---

## Foot Printing :

----

* [Tool used : Exiftool](https://exiftool.org/)
ExifTool is a free and opensource ❤ ; for reading writing and manipulating image , audio video and any media file metadata ! it's a toold used in the CTFs (especially steganography) and also in the forensics fields , to search in the meddia files 
Nasa , FBI also used this tool , the image hosting site [Flickr](https://www.flickr.com/) uses exiftool to parse the metadata from uploaded images .
----
* Device and Type of camera used :

As we see in the picture : the device and the lens used to take the picture is iPhone 8 back camera 3.99mm f/1.8 !
And the picture was modified with Adobe photoshop in a Macintosh device , so exiftool give us a banch of information about this type of files(media files) 
----
<img src="img/metadaident.png" alt="metadatainfo">
----

* [GPS coordination of the picture :](https://www.google.tn/maps/@63.6834439,-19.5310989,3a,75y,89.38h,94.5t/data=!3m8!1e1!3m6!1sAF1QipNmAJ0JSbX5B7DxzMfZ427ABT6-l_Uj_npgDKUg!2e10!3e11!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNmAJ0JSbX5B7DxzMfZ427ABT6-l_Uj_npgDKUg%3Dw203-h100-k-no-pi0-ya338.61856-ro-0-fo100!7i8704!8i4352?hl=fr&authuser=0)
----
<img src="img/Gpscoordination.png" alt="GPScoord">
----
<img src="img/location.png" alt="GPScoord">

----

[Link GPS blassa](https://www.google.tn/maps/@63.6834439,-19.5310989,3a,37.5y,215.38h,77.33t/data=!3m8!1e1!3m6!1sAF1QipNmAJ0JSbX5B7DxzMfZ427ABT6-l_Uj_npgDKUg!2e10!3e11!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNmAJ0JSbX5B7DxzMfZ427ABT6-l_Uj_npgDKUg%3Dw203-h100-k-no-pi0-ya338.61856-ro-0-fo100!7i8704!8i4352?hl=fr&authuser=0)
* Image GPS from google map : taken by [Conor McMonagle](https://www.google.com/maps/contrib/111003605763254555744/photos/@33.8819669,9.560764,6z/data=!4m3!8m2!3m1!1e1)
<img src="img/imagegpsgmap.png" alt="GPS image">

----

* After a little bit of search i found the same picture perspective in Google map 
 Krossa vom Valahnúkur 3 
----
 <img src="img/gpsSame.png" alt="same GPS location">
----
[this is a link of the exact picture](https://www.google.tn/maps/@63.68365,-19.525108,3a,75y,90t/data=!3m8!1e2!3m6!1sAF1QipNmqcjcEC-GrYooLqomDAF0GMrDbrRXj4Ycwh4j!2e10!3e12!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNmqcjcEC-GrYooLqomDAF0GMrDbrRXj4Ycwh4j%3Dw203-h152-k-no!7i2816!8i2112?hl=fr&authuser=0)

---

## Reconnaissance:

----
Run Whois against [esprit.tn](https://esprit.tn) mention the registrar of the website. take screenshot and
include it in the report.
----
<!-- .slide: data-background="./img/reconesprit.png" -->
<!-- <img src="img/reconesprit.png" alt="recon esprit"> -->
----
As we see in the screenshot the registrar is Globalnet (company) and the the administrator maybe  faycal.lbabda 
mentioned in the admin email .
----
<!-- .slide: data-background="./img/secondrecon.png" -->
<!-- <img src="img/secondrecon.png" alt="otehr recon img"> -->
---

## SeQueL Injection:

----

  ### Google Dorking :

----

  The first step that any good hacker is gonna take when they're about to hack somebody as we'r gonna try and learn as much possible information about their target as they can , this is often referred to as recon or u might see is a footprinting and information gathering because the more u know about ur target the better u can hack and later on with other hacking techniques , now a big reason why what i amm doing is not illegal is because we're doing a passive recon which in most cases (unless for me) means that we're just trying to get information that's been made public ! so we can make that with a google search and we call that google dorking

----

  By the way , we're hoping as hackers is that this information was made public by accident ! so one may accidentallly exposed their passwords or maybe left their configuration (dotenv files) shared to the others !!
----
  Let's go :
  first of all we need to gather a lot of information , So the Gathering process we just seach with the options in google
----

<img src="img/s1.png" alt="gathering">

----

<img src="img/s2.png" alt="gathering">

----

<img src="img/s3.png" alt="gathering">

----
  


  * What did you put in the search bar to find the login page?
  > Google Query : intitle:"admin" inurl:"login" after:2018
  also for the country u can just go in the search parametre in google and select the country (i choosed tunisia)
----

  * What did you type in the login field? Where you able to log in as an admin to any or locate its database? Take a screenshot and include it in the report.

----  

  Here after we found the login page , after a little bit of search we found a wamp server for this login page (it's a company based in tunisia ) then i found the logs files for this website !! after a hard try in searching i found the password in plaint text in the dotenv file (configuration of  the app) a default password for the admin , this password is always allowed in production!!

----

  login = admin 
  password = elmoudir123
  Find Log Files with Passwords

----

The next step will be to search for files of the .LOG type. Searching for LOG files will allow us to look for clues about what the credentials to the system or various user or admin accounts might be.

----

The dork we'll be using to do this is as follows.

>allintext:password filetype:log after:2018

in the same site 

> site:sotixxxx.xxx

----

<img src="img/loginpage.png" alt="gathering">

----

  the company based in tunisia and their main work is the manipulation of a lot of tunisian industries (electricity)

----

<img src="img/adminpanel1.png" alt="gathering">

----

<img src="img/adminpanel2.png" alt="gathering">
----

  * How can we defend again this attack : 
  This maybe don't be an attack !! any noob or a script kiddie in the field of the cybersecurity can destroy the data or manipulate these apps with the admin privileges  ; so i think there is some principles we should take :

----
  
    * [x] txt – make sure that Google is blocked from ALL sensitive directories
    * [x] Go Dork yourself – easy enough, use the site: operator and some Dorks on your own site
    like : site:"nader.me" inurl:"login" intitle="admin" 
    * [x] [Google Hack HoneyPot](http://ghh.sourceforge.net/) – another tool that you can use
    * [x] [Pentest-tools](https://pentest-tools.com/information-gathering/google-hacking) also has a tool to dig into a specific domain
    * [x] Avoid putting sensitive information online
    * [x] Keep software up to date
    * [x] Employ protection tools such as WordFence etc
    * [x] Use a VPN or IP filters on sensitive directories
    * [x] Password protected directories
    * [x] Make sure your hosting isn’t crap lol hhhhhh


---

## Social Engineering : 

----

### tHE MAIL IS a spam !! it's a fake domain name , the domain name myuniversity.edu don't exist 

----

<img src="img/whoisedu.png" alt="social">

---


## Analysing Network Traffic Using Wireshark

----

### Step 1 : Explore the Wireshark Interface

----

<img src="img/wiresharkstarter.png" alt="wireshark start page">

----

<img src="img/wireshark-options.png" alt="wireshark options">

----

### Step 2 : With the data being transmitted

----

  * How would you refer to data at layer 2 (with the OSI model)? :
  Layer 2 : Frames = Data link layer and that refers to 
  * How would you refer to data at layer 3 (with the OSI model)?
  layer 3 : Packets : Network 

----

  * How would you refer to data at layer 4 if TCP is used (with the OSI model)?
  in both TCP and OSI layer 4 is the same in the wireshark presentation!

  Layer 4 is the application layer

----

  <img src="img/layers.png" alt="layers in TCP/OSI model">

----

### Step 3 : Inspect DNS Requests – Part 1

----

<img src="img/dns1.png" alt="DNS1">

----

  What is the IP address of the requesting computer? 
  10.0.2.15
  What is the IP address of the DNS server used by this computer?
  209.18.47.62

 What site does this request look up? 
 www.umuc.edu

----

### Step 4 : Inspect DNS Responses

----

<img src="img/dns4.png" alt="dns">

----

How many IP addresses are assigned
to the site? 
4 ip addresses 

what are these IP addresses? 
the web servers generally , websites in those days are very consumed so to balance the huge consumption of theses websites the provider create multiple servers and Loadbalancer to manage the consumption of their services .

----

What protocol is the DNS protocol
implemented on top of? What is the port number used in that protocol?
port 53

The protocol is the DNS domain name server protocol !

<img src="img/dnsfilt.png" alt="filtering by port">

----

What packet list is displayed with this filter?
This List is the packets of DNS  ! 

----

### Step 5: Inspect DNS Requests – Part 2

----

What website does the DNS request found by the search?
mit.edu
 What is the IP address that was
found? 
104.125.30.202

----

<img src="img/dnsmit.png" alt="filtering by port">

----

### Step 6: Inspect HTTP Requests – Part 1

----

<img src="img/httpfilt.png" alt="filtering http">

----

What protocol implemented on top of HTTP is used in this capture?
OCSP : 
Research this protocol on Internet and briefly describe what it is used for.
OCSP verifies whether user certificates are valid. OCSP uses OCSP responders to determine the revocation status of an X.509 client certificate. The OCSP responder does its verification in real time by aggregating certificate validation data and responding to an OCSP request for a particular certificate. OCSP has a bit less overhead than CRL revocation. You do not have to keep downloading CRLs at the client side to maintain up-to-date certificate status information.

----

<img src="img/ocsp.png" alt="ocsp protocol">

----

Certificate Revocation is used within PKI (Public Key Infrastructure) to instruct the client that the certificate can no longer be trusted. This is required in scenarios where the private key has been compromised.

----

### Step 7: Inspect HTTP Requests – Part 2

----
What is the IP address of the request's destination?
23.49.176.128

 What is the HTTP
response code?
response code : [302](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/302)  


----

### Step 8: Inspect HTTP Requests – Part 3

----

What HTTP operation does this request invoke? 
This is a GET request 
What is its
destination?
23.49.176.128

----

What is the HTTP response code? 
[200](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/200)
What is the length of the returned data file?
1598

----

<img src="img/httpstep8.png" alt="ocsp protocol">

----

 What is the
extra header tucked on top of this response?
cookies and http version 1.1

----

<img src="img/httpheader7.png" alt="ocsp protocol">

----

### Step 9: Inspect IP Header

----

<img src="img/httpstep9.png" alt="ocsp protocol">

----

What is the total length of the IP header?
405

<img src="img/httpstep91.png" alt="ocsp protocol">

----

 What is the request's Time to Live?
0.031251429

<img src="img/httpstep92.png" alt="ocsp protocol">

----


### Step 10: Inspect TCP Header

----

In the packet detail pane of packet #3512, expand the Transmission Control Protocol
header.

What are the source and destination ports in the TCP header? 
src port : 35772
destination port : 80

----

What is the TCP sequence
number for this request?

sequence number : 1
 What is the sliding window size?

the window size is 64240 with no scaling 

<img src="img/tcpheader.png" alt="ocsp protocol">


---

### Synopsis:

----
* Introduction
* Footprinting & Information Gathering
* Reconnaissance
* Dorking and searching 
* Social Engineering
* Traffic Analysis


---

<!-- .slide: data-background-video="https://cdn.flixel.com/flixel/qqtfgw4xr6vmvay3hx5e.hd.mp4" data-background-video-loop="loop" data-background-video-muted -->

# EAT ,
# SLEEP , 
# HACK !
## REPEAT 😴 !

---
  ## Open Space 
  You can reach me in github : [!git/nerosiar](https://github.com/nerosiar)
  I have some projects and presentations !! like [intro in the web development](https://intro-webdev.vercel.app) , and [my black codium extension](https://open-vsx.org/extension/nerosiar/just-black) , and [a Cloud presentation !!](https://github.com/nerosiar)

  If u see that this report can be a presentation either u know how to reach me 😁 😁 !! 

  Your Feedback is Important !! 
<script src="https://www.hackthebox.eu/badge/444664"></script>
<img src="http://www.hackthebox.eu/badge/image/444664" alt="Hack The Box">